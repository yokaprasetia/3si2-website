<?php
class Admin extends CI_Controller
{
    // Halaman Beranda Utama
    public function index()
    {
        $data['judul'] = "APPINFO - Beranda Utama";

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/index');
        $this->load->view('templates/footer');
    }

    // Halaman Daftar Mahasiswa
    public function daftarMahasiswa()
    {
        $data['judul'] = "APPINFO - Daftar Mahasiswa";
        $data['mahasiswa'] = $this->Mahasiswa_model->get_data();
        if ($this->input->post('keyword')) {
            $data['mahasiswa'] = $this->Mahasiswa_model->cariDataMahasiswa();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/mahasiswa/daftarMahasiswa', $data);
        $this->load->view('templates/footer');
    }

    // Menampilkan detail data
    public function detail($id)
    {
        $data['judul'] = "APPINFO - Detail Mahasiswa";
        $data['detail'] = $this->Mahasiswa_model->detail_data($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/mahasiswa/detailMahasiswa', $data);
        $this->load->view('templates/footer');
    }

    // Halaman Edit Mahasiswa
    public function editMahasiswa()
    {
        $data['judul'] = "APPINFO - Edit Mahasiswa";
        $data['mahasiswa'] = $this->Mahasiswa_model->get_data();
        if ($this->input->post('keyword')) {
            $data['mahasiswa'] = $this->Mahasiswa_model->cariDataMahasiswa();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/mahasiswa/editMahasiswa', $data);
        $this->load->view('templates/footer');
    }

    // Halaman Create Mahasiswa
    public function createMahasiswa()
    {
        $data['judul'] = "APPINFO - Add Mahasiswa";
        $data['jenisKelamin'] = ['Laki - Laki', 'Perempuan'];

        $this->form_validation->set_rules('nim', 'NIM', 'required|numeric');
        $this->form_validation->set_rules('namaLengkap', 'Nama', 'required');
        $this->form_validation->set_rules('namaPanggilan', 'Nama Panggilan', 'required');
        $this->form_validation->set_rules('tempatLahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggalLahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('asalDaerah', 'Asal Daerah', 'required');
        $this->form_validation->set_rules('noHp', 'Nomor HP', 'required|numeric');
        $this->form_validation->set_rules('hobi', 'Hobi', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('pages/mahasiswa/createMahasiswa', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Mahasiswa_model->create_data();
            $this->session->set_flashdata('notif', 'Ditambahkan');
            redirect('admin/editMahasiswa');
        }
    }

    // Halaman Update Mahasiswa
    public function updateMahasiswa($id = NULL)
    {
        $data['judul'] = "APPINFO - Update Mahasiswa";
        $data['mahasiswa'] = $this->Mahasiswa_model->detail_data($id);
        $data['jenisKelamin'] = ['Laki - Laki', 'Perempuan'];

        $this->form_validation->set_rules('nim', 'NIM', 'required|numeric');
        $this->form_validation->set_rules('namaLengkap', 'Nama', 'required');
        $this->form_validation->set_rules('namaPanggilan', 'Nama Panggilan', 'required');
        $this->form_validation->set_rules('tempatLahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggalLahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('asalDaerah', 'Asal Daerah', 'required');
        $this->form_validation->set_rules('noHp', 'Nomor HP', 'required|numeric');
        $this->form_validation->set_rules('hobi', 'Hobi', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('pages/mahasiswa/updateMahasiswa', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Mahasiswa_model->update_data();
            $this->session->set_flashdata('notif', 'Diupdate');
            redirect('admin/editMahasiswa');
        }
    }

    public function delete($id = NULL)
    {
        $this->Mahasiswa_model->delete_data($id);

        $this->session->set_flashdata('notif', 'Dihapus');
        redirect('admin/editMahasiswa');
    }
}
