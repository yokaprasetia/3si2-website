<?php
class Ppk extends CI_Controller
{

    // Menampilkan website Json
    public function json()
    {
        $data['judul'] = "APPINFO - Json Website";

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/rss/websiteJson');
        $this->load->view('templates/footer');
    }

    // Menampilkan website Xml
    public function xml()
    {
        $data['judul'] = "APPINFO - Xml Website";

        $this->load->view('pages/rss/headerXml', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('pages/rss/websiteXml');
        $this->load->view('templates/footer');
    }
}
