<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Update Data Mahasiswa</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <!-- <form method="POST" action="" enctype="multipart/form-data"> -->
        <?php echo form_open_multipart(''); ?>

        <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $mahasiswa['id']; ?>">

        <div class="form-group row">
            <label for="nim" class="col-sm-2 col-form-label font-weight-normal">NIM</label>
            <div class="col-sm-10">
                <input type="text" name="nim" class="form-control" id="nim" value="<?php echo $mahasiswa['nim']; ?>">
                <small class="form-text text-warning"><?php echo form_error('nim'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="namaLengkap" class="col-sm-2 col-form-label font-weight-normal">Nama Lengkap</label>
            <div class="col-sm-10">
                <input type="text" name="namaLengkap" class="form-control" id="namaLengkap" value="<?php echo $mahasiswa['namaLengkap']; ?>">
                <small class="form-text text-warning"><?php echo form_error('namaLengkap'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="namaPanggilan" class="col-sm-2 col-form-label font-weight-normal">Nama Panggilan</label>
            <div class="col-sm-10">
                <input type="text" name="namaPanggilan" class="form-control" id="namaPanggilan" value="<?php echo $mahasiswa['namaPanggilan']; ?>">
                <small class="form-text text-warning"><?php echo form_error('namaPanggilan'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="tempatLahir" class="col-sm-2 col-form-label font-weight-normal">Tempat Lahir</label>
            <div class="col-sm-10">
                <input type="text" name="tempatLahir" class="form-control" id="tempatLahir" value="<?php echo $mahasiswa['tempatLahir']; ?>">
                <small class="form-text text-warning"><?php echo form_error('tempatLahir'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="tanggalLahir" class="col-sm-2 col-form-label font-weight-normal">Tanggal Lahir</label>
            <div class="col-sm-10">
                <input type="date" name="tanggalLahir" class="form-control" id="tanggalLahir" value="<?php echo $mahasiswa['tanggalLahir']; ?>">
                <small class="form-text text-warning"><?php echo form_error('tanggalLahir'); ?></small>
            </div>
        </div>

        <fieldset class="form-group row">
            <legend class="col-form-label col-sm-2 float-sm-left pt-0">Jenis Kelamin</legend>
            <div class="col-sm-10">
                <?php foreach ($jenisKelamin as $jk) : ?>
                    <?php if ($jk == $mahasiswa['jenisKelamin']) : ?>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="<?php echo $jk ?>" value="<?php echo $jk; ?>" checked>
                            <label class="form-check-label" for="<?php echo $jk ?>">
                                <?php echo $jk; ?>
                            </label>
                        </div>
                    <?php else : ?>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenisKelamin" id="<?php echo $jk ?>" value="<?php echo $jk; ?>">
                            <label class="form-check-label" for="<?php echo $jk ?>">
                                <?php echo $jk; ?>
                            </label>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </fieldset>

        <div class="form-group row">
            <label for="asalDaerah" class="col-sm-2 col-form-label font-weight-normal">Asal Daerah</label>
            <div class="col-sm-10">
                <input type="text" name="asalDaerah" class="form-control" id="asalDaerah" value="<?php echo $mahasiswa['asalDaerah']; ?>">
                <small class="form-text text-warning"><?php echo form_error('asalDaerah'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="noHp" class="col-sm-2 col-form-label font-weight-normal">Nomor HP</label>
            <div class="col-sm-10">
                <input type="text" name="noHp" class="form-control" id="noHp" value="<?php echo $mahasiswa['noHp']; ?>">
                <small class="form-text text-warning"><?php echo form_error('noHp'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="hobi" class="col-sm-2 col-form-label font-weight-normal">Hobi</label>
            <div class="col-sm-10">
                <input type="text" name="hobi" class="form-control" id="hobi" value="<?php echo $mahasiswa['hobi']; ?>">
                <small class="form-text text-warning"><?php echo form_error('hobi'); ?></small>
            </div>
        </div>

        <div class="form-group row">
            <label for="foto" class="col-sm-2 col-form-label font-weight-normal">Foto</label>
            <div class="col-sm-10">
                <div class="custom-file">
                    <input type="file" id="foto" name="foto">
                </div>
            </div>
        </div>

        <button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-info">Update</button>

        <?php echo form_close(); ?>
        <!-- </form> -->

    </section>
</div>
<!-- /.content-wrapper -->