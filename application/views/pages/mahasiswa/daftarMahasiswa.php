<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Daftar Mahasiswa</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-6">
                <form action="" method="POST">
                    <div class="input-group mb-4">
                        <input type="text" name="keyword" class="form-control" placeholder="Cari Data Mahasiswa">
                        <div class="input-group-append">
                            <button class="btn btn-outline-info" type="submit">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">NIM</th>
                    <th scope="col">NAMA LENGKAP</th>
                    <th scope="col">TEMPAT, TANGGAL LAHIR </th>
                    <th scope="col">ASAL DAERAH</th>
                    <th scope="col">AKSI</th>
                </tr>
            </thead>

            <tbody>
                <?php $no = 1 ?>
                <?php foreach ($mahasiswa as $mhs) : ?>
                    <tr>
                        <th scope="row"><?php echo $no ?></th>
                        <td><?php echo $mhs['nim']; ?></td>
                        <td><?php echo $mhs['namaLengkap']; ?></td>
                        <td><?php echo $mhs['tempatLahir']; ?>, <?php echo $mhs['tanggalLahir'] ?></td>
                        <td><?php echo $mhs['asalDaerah']; ?></td>
                        <td>
                            <?php echo anchor('admin/detail/' . $mhs['id'], '<div class="btn btn-info btn-sm"><i class="fa fa-search-plus"></i></div>') ?>
                        </td>
                    </tr>
                    <?php $no++ ?>
                <?php endforeach; ?>
            </tbody>
        </table>

        <!-- alert -->
        <?php if (empty($mahasiswa)) : ?>
            <div class="alert alert-danger" role="alert">
                <i>Data mahasiswa tidak ditemukan!</i>
            </div>
        <?php endif; ?>

    </section>
</div>
<!-- /.content-wrapper -->