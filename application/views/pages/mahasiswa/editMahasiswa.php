<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Edit Mahasiswa</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col mb-2">
                <a href="<?php echo base_url(); ?>admin/createMahasiswa" class="btn btn-info mb-2">Tambah Data Mahasiswa</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <form action="" method="POST">
                    <div class="input-group mb-4">
                        <input type="text" name="keyword" class="form-control" placeholder="Cari Data Mahasiswa">
                        <div class="input-group-append">
                            <button class="btn btn-outline-info" type="submit">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php if ($this->session->flashdata('notif')) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Mahasiswa<strong> Berhasil </strong><?php echo $this->session->flashdata('notif'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">NIM</th>
                    <th scope="col">NAMA LENGKAP</th>
                    <th scope="col">ASAL DAERAH</th>
                    <th scope="col">AKSI</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1 ?>
                <?php foreach ($mahasiswa as $mhs) : ?>
                    <tr>
                        <th scope="row"><?php echo $no ?></th>
                        <td><?php echo $mhs['nim']; ?></td>
                        <td><?php echo $mhs['namaLengkap']; ?></td>
                        <td><?php echo $mhs['asalDaerah']; ?></td>
                        <td>
                            <a href="updateMahasiswa/<?php echo $mhs['id']; ?>" class="btn btn-warning btn-sm"> <i class="fas fa-edit"></i></a>
                            <a href="delete/<?php echo $mhs['id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus <?php echo $mhs['namaLengkap'] ?>?')"> <i class="fas fa-trash-alt"></i></a>

                        </td>
                    </tr>
                    <?php $no++ ?>
                <?php endforeach; ?>
            </tbody>
        </table>

        <!-- alert -->
        <?php if (empty($mahasiswa)) : ?>
            <div class="alert alert-danger" role="alert">
                <i>Data mahasiswa tidak ditemukan!</i>
            </div>
        <?php endif; ?>

    </section>
</div>
<!-- /.content-wrapper -->

<?php
if (isset($_SESSION['notif'])) {
    unset($_SESSION['notif']);
}
?>