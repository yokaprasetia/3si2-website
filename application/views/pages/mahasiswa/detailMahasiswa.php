<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Detail Mahasiswa</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <div class="card">
            <div class="card-header">
                <?php echo $detail['namaLengkap'] ?>
            </div>
            <div class="card-body">
                <table class="table">
                    </thead>
                    <tbody>

                        <tr>
                            <th>NIM</th>
                            <td>::</td>
                            <td><?php echo $detail['nim']; ?></td>
                        </tr>
                        <tr>
                            <th>Nama Lengkap</th>
                            <td>::</td>
                            <td><?php echo $detail['namaLengkap']; ?></td>
                        </tr>
                        <tr>
                            <th>Nama User</th>
                            <td>::</td>
                            <td><?php echo $detail['namaUser']; ?></td>
                        </tr>
                        <tr>
                            <th>Nama Panggilan</th>
                            <td>::</td>
                            <td><?php echo $detail['namaPanggilan']; ?></td>
                        </tr>
                        <tr>
                            <th>Tempat, Tanggal Lahir</th>
                            <td>::</td>
                            <td><?php echo $detail['tempatLahir']; ?>, <?php echo $detail['tanggalLahir']; ?></td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>::</td>
                            <td><?php echo $detail['jenisKelamin']; ?></td>
                        </tr>
                        <tr>
                            <th>Asal Daerah</th>
                            <td>::</td>
                            <td><?php echo $detail['asalDaerah']; ?></td>
                        </tr>
                        <tr>
                            <th>Nomor HP</th>
                            <td>::</td>
                            <td><?php echo $detail['noHp']; ?></td>
                        </tr>
                        <tr>
                            <th>Hobi</th>
                            <td>::</td>
                            <td><?php echo $detail['hobi']; ?></td>
                        </tr>
                        <tr>
                            <th>Foto</th>
                            <td>::</td>
                            <td><img style="width:200px;" class="img-thumbnail" src="<?php echo base_url(); ?>assets/foto/<?php echo $detail['foto']; ?>"></td>
                        </tr>
                    </tbody>
                </table>
                <a href="<?php echo base_url(); ?>admin/daftarMahasiswa" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->