<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $judul; ?></title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/adminlte.min.css">

    <script>
        function loadXMLDoc(filename) {
            if (window.ActiveXObject) {
                xhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } else {
                xhttp = new XMLHttpRequest();
            }
            xhttp.open("GET", filename, false);
            try {
                xhttp.responseType = "msxml-document"
            } catch (err) {} // Helping IE11
            xhttp.send("");
            return xhttp.responseXML;
        }

        function displayResult() {
            $source = ['https://www.antaranews.com/rss/hukum.xml', 'https://www.antaranews.com/rss/sepakbola-liga-indonesia.xml', 'https://www.antaranews.com/rss/hiburan-musik.xml'];


            for ($i = 0; $i < 3; $i++) {
                xml = loadXMLDoc($source[$i]);
                xsl = loadXMLDoc('<?php echo base_url(); ?>assets/rss/dataXml.xsl');
                // code for IE
                if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
                    ex = xml.transformNode(xsl);
                    document.getElementById("example").innerHTML = ex;
                }
                // code for Chrome, Firefox, Opera, etc.
                else if (document.implementation && document.implementation.createDocument) {
                    xsltProcessor = new XSLTProcessor(xml);
                    xsltProcessor.importStylesheet(xsl);
                    resultDocument = xsltProcessor.transformToFragment(xml, document);
                    document.getElementById("example").appendChild(resultDocument);
                }

            }
        }
    </script>
</head>

<body id="example" onload="displayResult()" class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?php echo base_url(); ?>" class="nav-link">Beranda</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Sign Out</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->