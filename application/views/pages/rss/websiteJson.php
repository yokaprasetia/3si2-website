<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Online Report Website</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <?php
        $source = ['https://www.antaranews.com/rss/hukum.xml', 'https://www.antaranews.com/rss/sepakbola-liga-indonesia.xml', 'https://www.antaranews.com/rss/hiburan-musik.xml'];
        for ($i = 0; $i < 3; $i++) {
            $xml = simplexml_load_file($source[$i]);
            $json = json_encode($xml, JSON_PRETTY_PRINT);
            $data[$i] = json_decode($json, true);
        }
        ?>

        <h2 class="text-center mt-1">DAFTAR BERITA SEPUTAR INDONESIA</h2>
        <p class="text-center mb-3">This website is made by JSON</p>
        <div class="row row-cols-1 row-cols-md-3">
            <?php
            foreach ($data as $result1) :
                foreach ($result1 as $result2) :
                    foreach ($result2 as $item) :
                        if (is_array($item)) {
                            for ($i = 0; $i < 6; $i++) {
            ?>
                                <div class="col">
                                    <div class="card m-1">
                                        <img src="<?php echo $item[$i]['enclosure']['@attributes']['url']; ?>" class="card-img-top">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php echo $item[$i]['title'] ?></h5>
                                            <p class="card-text"><small class="text-muted"><?php echo $item[$i]['pubDate'] ?></small></p>
                                            <a href="<?php echo $item[$i]['link'] ?>" class="btn btn-primary btn-lg btn-block">Kunjungi Situs</a>
                                        </div>
                                    </div>
                                </div>
            <?php
                            }
                        }
                    endforeach;
                endforeach;
            endforeach;
            ?>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->