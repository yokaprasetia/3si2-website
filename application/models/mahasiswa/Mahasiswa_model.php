<?php
class Mahasiswa_model extends CI_Model
{

    // Menampilkan data informasi
    public function get_data()
    {
        $this->db->order_by('nim', 'ASC');
        return $this->db->get('datamahasiswa')->result_array();
    }

    // Memampilkan detail data
    public function detail_data($id = NULL)
    {
        // karena cuma 1 baris, gunakan row_array()
        return $this->db->get_where('datamahasiswa', ['id' => $id])->row_array();
    }

    // Membuat data baru
    public function create_data()
    {
        $namaLengkap = $this->input->post('namaLengkap', true);
        $namaUser = url_title($namaLengkap, '-', true);

        $foto = $_FILES['foto'];
        if ($foto = '') {
        } else {
            $config['upload_path'] = './assets/foto';
            $config['allowed_types'] = 'jpg|png';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                echo "Upload Gagal";
                die();
            } else {
                $foto = $this->upload->data('file_name');
            }
        }

        $data = [
            'id' => '',
            'nim' => $this->input->post('nim', true),
            'namaLengkap' => $this->input->post('namaLengkap', true),
            'namaUser' => $namaUser,
            'namaPanggilan' => $this->input->post('namaPanggilan', true),
            'tempatLahir' => $this->input->post('tempatLahir', true),
            'tanggalLahir' => $this->input->post('tanggalLahir', true),
            'jenisKelamin' => $this->input->post('jenisKelamin', true),
            'asalDaerah' => $this->input->post('asalDaerah', true),
            'noHp' => $this->input->post('noHp', true),
            'hobi' => $this->input->post('hobi', true),
            'foto' => $foto
        ];
        return $this->db->insert('datamahasiswa', $data);
    }


    // proses delete data
    public function delete_data($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('datamahasiswa');
    }

    public function update_data()
    {
        $namaLengkap = $this->input->post('namaLengkap', true);
        $namaUser = url_title($namaLengkap, '-', true);

        $foto = $_FILES['foto'];
        if ($foto = '') {
        } else {
            $config['upload_path'] = './assets/foto';
            $config['allowed_types'] = 'jpg|png';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                echo "Upload Gagal";
                die();
            } else {
                $foto = $this->upload->data('file_name');
            }
        }

        $data = [
            'id' => $this->input->post('id', true),
            'nim' => $this->input->post('nim', true),
            'namaLengkap' => $this->input->post('namaLengkap', true),
            'namaUser' => $namaUser,
            'namaPanggilan' => $this->input->post('namaPanggilan', true),
            'tempatLahir' => $this->input->post('tempatLahir', true),
            'tanggalLahir' => $this->input->post('tanggalLahir', true),
            'jenisKelamin' => $this->input->post('jenisKelamin', true),
            'asalDaerah' => $this->input->post('asalDaerah', true),
            'noHp' => $this->input->post('noHp', true),
            'hobi' => $this->input->post('hobi', true),
            'foto' => $foto
        ];
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update('datamahasiswa', $data);
    }

    public function cariDataMahasiswa()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nim', $keyword);
        $this->db->or_like('namaLengkap', $keyword);
        $this->db->or_like('asalDaerah', $keyword);
        $this->db->or_like('tempatLahir', $keyword);
        $this->db->or_like('tanggalLahir', $keyword);
        return $this->db->get('datamahasiswa')->result_array();
    }
}
